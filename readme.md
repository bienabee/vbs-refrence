![Bienabee Logo](images/bienabee_logo.png)

# Bienabee UFT Training - VBS Refrence #

This repository will serve as an introduction to Microsoft's Visual Basic Scripting by documenting and describing VBS concepts that can be applied and utilized within Micro Focus UFT. You will find a combination of VBS examples as well as links to useful online learning resources.

### What is VBS? ###

Visual Basic Script is a lightweight scripting language developed by Microsoft. The syntax and features of VBS are modled after Visual Basic programming language. VBS leverages the Component Object Model (COM) to interact with the system on which the script is running and can interact with most features of the Windows API.

* [Microsoft's Visual Basic Scripting Refrence](https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/vb-script/d1wf56tt%28v%3dvs.84%29)

### Who do I talk to? ###

* Bienabee Technologies  - [info@bienabee.com](mailto:info@bienabee.com)